"""Write a program to trace your subject mark. Your program should fulfill the following conditions:
If the subject mark is below 0 and above 100, print “error: mark should be between 0 and 100 only”
Students will fail in the subject if their mark is below 50.
Students will pass in the subject if they score 50 and above.
    If the subject mark is between 50 and 60, the grade student as good.
    If the subject mark is between 60 and 80, the grade student as very good.
    If the subject mark is between 80 and 100, the grade student as outstanding.
Make sure to print their mark in every statement to prove that the condition is fulfilled. Moreover, name, class, and section should be also displayed along with the marks and their grade.
"""
student_name = input("Enter Students Name:")
student_class = input("Enter Students Class:")
student_section = input("Enter Students Section:")
student_marks = int(input("Enter Students Marks:"))
if (student_marks>=0 and student_marks<=100):
    if (student_marks>=50):
        if (student_marks>=50 and student_marks<60):
            print("\nName Of Student:",student_name)
            print("Class:",student_class)
            print("Section:",student_section)
            print("Marks:",student_marks)
            print("Good")
        elif (student_marks>=60 and student_marks<80):
            print("\nName Of Student:",student_name)
            print("Class:",student_class)
            print("Section:",student_section)
            print("Marks:",student_marks)
            print("Very Good")
        else:
            print("\nName Of Student:",student_name)
            print("Class:",student_class)
            print("Section:",student_section)
            print("Marks:",student_marks)
            print("Outstanding")
    else:
        print("\nName Of Student:",student_name)
        print("Class:",student_class)
        print("Section:",student_section)
        print("Marks:",student_marks)
        print("Fail")
else:
    print("\nMarks:", student_marks)
    print("error: mark should be between 0 and 100 only")